<!DOCTYPE html>
<html>
<head>
	<title>Grafik</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha256-TQq84xX6vkwR0Qs1qH5ADkP+MvH0W+9E7TdHJsoIQiM=" crossorigin="anonymous"></script>
</head>
<body>
	<style type="text/css">
	body{
		font-family: roboto;
	}
 
	table{
		margin: 0px auto;
	}
	</style>
 
 
	<center>
		<h2>Grafik total yang di hafal</h2>
	</center>
 
 
	<?php 
	include 'koneksi.php';
	?>
 
	<div style="width: 800px;margin: 0px auto;">
		<canvas id="myChart"></canvas>
	</div>
 
	<br/>
	<br/>
 
	<table class="table wy-table-striped" style=width:50%;text-align:center;>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Ayat</th>
				<th>Arti</th>
                <th>Diturunkan</th>
                <th>manfaat</th>
                <th>hafal</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			$data = mysqli_query($koneksi,"select * from detail_surah");
			while($d=mysqli_fetch_array($data)){
				?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $d['nama']; ?></td>
					<td><?php echo $d['ayat']; ?></td>
					<td><?php echo $d['arti']; ?></td>
                    <td><?php echo $d['diturunkan']; ?></td>
                    <td><?php echo $d['manfaat']; ?></td>
                    <td><?php echo $d['hafal']; ?></td>
				</tr>
				<?php 
			}
			?>
		</tbody>
	</table>
 
 
	<script>
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["yes", "no"],
				datasets: [{
					label: '',
					data: [
					<?php 
					$jumlah_hafal = mysqli_query($koneksi,"select * from detail_surah where hafal='y'");
					echo mysqli_num_rows($jumlah_hafal);
					?>, 
					<?php 
					$jumlah_hafal = mysqli_query($koneksi,"select * from detail_surah where hafal='t'");
					echo mysqli_num_rows($jumlah_hafal);
					?>, 
					],
					backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(75, 192, 192, 0.2)'
					],
					borderColor: [
					'rgba(255,99,132,1)',
					'rgba(75, 192, 192, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
	</script>
</body>
</html>