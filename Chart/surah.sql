-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 06 Bulan Mei 2020 pada 06.07
-- Versi server: 10.1.28-MariaDB
-- Versi PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_surah`
--

CREATE TABLE `detail_surah` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ayat` int(11) NOT NULL,
  `arti` varchar(50) NOT NULL,
  `diturunkan` varchar(15) NOT NULL,
  `manfaat` varchar(1000) NOT NULL,
  `hafal` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_surah`
--

INSERT INTO `detail_surah` (`id`, `nama`, `ayat`, `arti`, `diturunkan`, `manfaat`, `hafal`) VALUES
(2, 'an-nas', 6, 'manusia', 'mekah', '0', 'y'),
(3, 'al-falaq', 5, 'waktu subuh', 'mekah', '0', 'y'),
(4, 'al-ikhlas', 4, '0', 'mekah', '0', 'y'),
(5, 'al-lahab', 5, 'gejolak api', 'mekah', '0', 'y'),
(6, 'an-nasr', 3, 'pertolongan', 'madinah', '0', 'y'),
(7, 'al-kafirun', 6, 'orang orang kafir', 'mekah', '0', 'y'),
(8, 'al-kausar', 3, 'nikmat yang berlimpah', 'mekah', '0', 'y'),
(9, 'al-maun', 7, 'barang barang yang berguna', 'mekah', '0', 'y'),
(10, 'al quraisy', 4, '0', 'mekah', '0', 't'),
(11, 'al-fil', 5, 'gajah', 'mekah', '0', 't'),
(12, 'al-humazah', 9, 'pengumpat', 'mekah', '0', 't'),
(13, 'al-asr', 3, 'masa/waktu', 'mekah', '0', 't'),
(14, 'at-takasur', 8, 'bermegah megahan', 'mekah', '0', 't'),
(15, 'al-qariah', 11, 'peristiwa menggetarkan', 'mekah', '0', 't'),
(16, 'al-adiyat', 11, 'kuda perang', 'mekah', '0', 't'),
(17, 'al-zalzalah', 8, 'kegoncangan', 'madinah', '0', 't'),
(18, 'al-bayinah', 8, 'pembuktian', 'madinah', '0', 't'),
(19, 'al-qadr', 5, 'kemuliaan', 'madinah', '0', 't'),
(20, 'al-alaq', 19, 'segumpal darah', 'mekah', '0', 't'),
(21, 'at-tin', 8, 'buah tin', 'mekah', '0', 't');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_surah`
--
ALTER TABLE `detail_surah`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_surah`
--
ALTER TABLE `detail_surah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
